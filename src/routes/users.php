<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;

// Get All Users

$app->get('/api/users', function(Request $request, Response $response){

  $sql = "SELECT * FROM users";

    try{
      // get the database obj
      $db = new db();
      // connect
      $db = $db->connect();

      $stmt = $db->query($sql);
      $users = $stmt->fetchAll(PDO::FETCH_OBJ);
      $db = null;
      echo json_encode($users);

    } catch(PDOException $e) {

      // OUtput Error as Json
      echo '{"error": {"text"; '.$e->getMessage().'}}';

}

});
